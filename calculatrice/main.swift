//
//  main.swift
//  calculatrice
//
//  Created by Christopher Jamme de Lagoutine on 13/12/2019.
//  Copyright © 2019 Christopher Jamme de Lagoutine. All rights reserved.
//

import Foundation
let supportedOperations = ["+", "-", "/", "*", "%"]

func    getOperandNumbers(forExpression expression: String, withOperator oper : String.Element) -> (Int, Int)
{

    let operandSides = expression.split(separator: oper)
    return (Int(String(operandSides.first!))!, Int(String(operandSides.last!))!)
}

func    getOperatorSymbols(forExpression expression: String) -> String
{
    return expression.filter { t in
        supportedOperations.contains(String(t))
    }
}

func    isExpressionValid(_ expression : String) -> Bool
{
    return expression.filter { element in
        !element.isNumber && !supportedOperations.contains(String(element))
    }.isEmpty
}

func    trimWhiteSpaces(input: String?) -> String
{
    return input!.filter { element in !" ".contains(element) }
}

func    performOperation(_ oper: String.Element, lhs : Int, rhs: Int)
{
    switch oper {
       case "+":
           print(lhs + rhs)
           break
       case "-":
           print(lhs - rhs)
       case "/":
           print(lhs / rhs)
       case "*":
           print(lhs * rhs)
       case "%":
           print(lhs % rhs)
       default:
            break;
       }
}

while (true)
{
    print("Enter the expression to evaluate: ", separator: "", terminator: "")
    let input = readLine()
    let trimmedExpression = trimWhiteSpaces(input: input)
    
    if (!isExpressionValid(trimmedExpression))
    {
        print("You have to provide only numbers.")
        continue
    }
        
    let operatorSymbols = getOperatorSymbols(forExpression: trimmedExpression)
    
    if (operatorSymbols.count != 1) {
        print("You have to provide one and only one operator from : +,-,/,*,%.")
        continue
    }
    
    let oper = operatorSymbols.first!
    
    let (lhs, rhs) = getOperandNumbers(forExpression: trimmedExpression, withOperator: oper)
 
    performOperation(oper, lhs: lhs, rhs: rhs)
}

